Задание 4:
1. Найти полеты (flights) из аэропорта Казани в Краснодар
2. Найти все полеты из Москвы (все аэропорты) за 25 минут (за какую дату и час придумайте сами)
3. Показать все полеты в одной временной зоне (полеты и аэропорты отобразить вместе)

Решение:
--task1
select *
from flights
where departure_airport = (select airport_code from airports_data where city ->> 'ru' = 'Казань')
  and
    arrival_airport=(
select airport_code
from airports_data
where city ->> 'ru' ='Магнитогорск');

--task2
select *
from flights f
where ((select city ->> 'ru'
from airports_data
where airport_code = f.departure_airport) = 'Москва')
  and (actual_departure between '2017-07-16 19:08:00+03'
  and timestamp '2017-07-16 19:08:00+03' +
    25 * interval '1 minutes')

--task3
select *
from airports_data d
         join flights f on d.airport_code = f.departure_airport and d.timezone = 'Europe/Moscow';
