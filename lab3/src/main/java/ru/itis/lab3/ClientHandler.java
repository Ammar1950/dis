package ru.itis.lab3;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClientHandler extends Thread {
    private Socket clientSocket;
    private Map<String, List<Message>> queue;
    private Map<String, Writer> clientStream;

    public ClientHandler(Socket clientSocket, Map<String, List<Message>> queue, Map<String, Writer> clientStream) {
        this.clientSocket = clientSocket;
        this.queue = queue;
        this.clientStream = clientStream;
    }

    @Override
    public void run() {
        process();
    }

    public void process() {

        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String name = in.readLine().trim();
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            out.write("You got connected as: " + name + "\n");
            out.flush();
            // Send latest messages
            clientStream.put(name, out);
            List<Message> lastMessage = queue.get(name);
            if (lastMessage != null && lastMessage.size() > 0) {
                for (Message message : lastMessage) {
                    out.write(message.toString());
                }
                out.flush();
            }

            while (clientSocket.isConnected()) {
                String msg = in.readLine();

                //Change from String to Class(Message)
                Message message = new Message().writeMessage(msg, name);
                Writer dstWrite = clientStream.get(message.getDstClientName());
                //Write to user if EXISTS
                if (dstWrite != null) {
                    dstWrite.write(message.toString());
                    dstWrite.flush();
                }
                //IF not save it in lst so that when user connects , he gets the msg
                else {
                    List<Message> lst = queue.get(message.getDstClientName());
                    if (lst != null) {
                        lst.add(message);
                    } else {
                        lst = new ArrayList<>();
                        lst.add(message);
                        queue.put(message.getDstClientName(), lst);
                    }
                }
                out.flush();
            }
            //Close all thread  & removing user data from server
            clientStream.remove(name);
            lastMessage.clear();
            queue.remove(name);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     *      try {
     *                     in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
     *                     out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
     *
     *                     String word = in.readLine();
     *                     System.out.println(word);
     *                     out.write("Hello this is server :" + word + "\n");
     *                     out.flush();
     *                 }finally {
     *                     clientSocket.close();
     *                     in.close();
     *                     out.close();
     *                 }
     */
}
