package ru.itis.lab3;



import java.text.SimpleDateFormat;
import java.util.Date;

//@Data
public class Message {

    private String clientName;
    private String dstClientName;
    private String messageText;
    private Date msgDate;

    public Message(String dstClientName, String clientName, String messageText, Date msgDate) {
        this.dstClientName = dstClientName;
        this.clientName = clientName;
        this.messageText = messageText;
        this.msgDate = msgDate;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getDstClientName() {
        return dstClientName;
    }

    public void setDstClientName(String dstClientName) {
        this.dstClientName = dstClientName;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public Date getMsgDate() {
        return msgDate;
    }

    public void setMsgDate(Date msgDate) {
        this.msgDate = msgDate;
    }

    public Message() {

    }

    //From The message String to Class
    public Message writeMessage(String msg, String clientName) {

        String[] str = msg.split(",");
        this.dstClientName = str[0];
        this.messageText = str[1];
        this.clientName = clientName;
        this.msgDate = new Date();
        return this;
    }

    @Override
    public String toString() {
        return ("[" + new SimpleDateFormat("dd.MM.YYYY HH.mm").format(this.getMsgDate()) + "]"
                + this.getClientName() + ": " + this.getMessageText() + "\n");
    }
}
