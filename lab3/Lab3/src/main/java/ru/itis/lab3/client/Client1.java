package ru.itis.lab3.client;

import java.io.*;
import java.net.Socket;

public class Client1 {
    private static Socket clientSocket;
    private static BufferedReader reader;
    private static BufferedReader in;
    private static BufferedWriter out;

    public static void main(String[] args) {
        try {
            try {

                clientSocket = new Socket("127.0.0.1", 10000);
                reader = new BufferedReader(new InputStreamReader(System.in));
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                System.out.println("Enter your name:");
                String name = reader.readLine();

                //Send name to  server
                out.write(name + "\n");
                out.flush();

                //Client Reader
                Thread readFromServer = new Thread(() -> {
                    while (in != null) {
                        try {
                            String msg = in.readLine();
                            System.out.println(msg);
                        } catch (IOException e) {
                            e.printStackTrace();
                            break;
                        }
                    }
                });
                readFromServer.start();


                String message = "";
                do {
                    message = reader.readLine();
                    if (!message.equals("\\q")) {
                        out.write(message+"\n");
                        out.flush();
                    }
                } while (!message.equals("\\q"));
            } finally {
                System.out.println("Клиент был закрыт...");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}
