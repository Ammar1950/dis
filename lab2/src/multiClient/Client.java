package multiClient;

import java.io.*;
import java.net.Socket;

public class Client extends Thread{

    private static Socket clientSocket;
    private static BufferedReader reader;
    private static BufferedReader in;
    private static BufferedWriter out;

    public static void main(String[] args) {
        try {
            try {
                clientSocket = new Socket("127.0.0.1", 100);
                reader = new BufferedReader(new InputStreamReader(System.in));
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                System.out.println("Вы что-то хотели сказать? Введите это здесь:");
                while (true){
                    String word = reader.readLine();
                    if (word.equalsIgnoreCase("Quit")){
                        out.write("Quit");
                        break;
                    }
                    System.out.println(word);
                    out.write(word);
                    String answer = in.readLine();
                    System.out.println(answer);
                    if (answer.equalsIgnoreCase("Matches")){
                        System.out.println(answer);
                        break;
                    }else {
                        System.out.println(answer);
                    }
                }
            } finally {
                System.out.println("Клиент был закрыт...");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }

    }

}

