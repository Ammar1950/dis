package multiClient;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static Socket clientSocket;
    private static ServerSocket server;

    public static void main(String[] args3) {
        try {
            try {
                server = new ServerSocket(100);
                System.out.println("Сервер запущен!");
                while (true) {
                    clientSocket = server.accept();
                    System.out.println(clientSocket+": got connected");
                    new SubServer(clientSocket).start();
                }
            } finally {
                System.out.println("Сервер закрыт!");
                server.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}

