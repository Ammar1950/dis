package multiClient;

import java.io.*;
import java.net.Socket;
import java.util.Random;

public class SubServer extends Thread {

    private Socket client;
    private BufferedReader in;
    private BufferedWriter out;
    int number;

    public SubServer(Socket clientSocket) {
        this.client = clientSocket;
        number = new Random().nextInt();
    }

    @Override
    public void run() {
        try {
            in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
            try {
                while (true) {
                    String word = in.readLine();
                    if (word.equalsIgnoreCase("Quit")) {
                        break;
                    }
                    if (number == Integer.parseInt(word)) {
                        out.write("Matches");
                        break;
                    } else {
                        out.write("Doesn't match");
                    }
                }
            } catch (IOException e) {
                System.out.println(e);
            } finally {
                client.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
