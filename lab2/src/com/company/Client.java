package com.company;

import java.io.*;
import java.net.Socket;

public class Client {

    private static Socket clientSocket;
    private static BufferedReader reader;
    private static BufferedReader in;
    private static BufferedWriter out;

    public static void main(String[] args) {
        try {
            try {

                /*for(3)
                * write word
                * flush
                * if matches
                * sout correct break;
                * else is not correct
                * */
                clientSocket = new Socket("127.0.0.1", 10000);
                reader = new BufferedReader(new InputStreamReader(System.in));
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                System.out.println("Вы что-то хотели сказать? Введите это здесь:");

/*
* while when the user writes quit or get the correct answer
* if quit -> write to the server QUIT -> break
* else if matches -> sout(SERVER'S REPLY) -> break
* else sout(SERVER'S REPLY)*/

                for (int i = 0; i <3 ; i++) {
                    String word = reader.readLine();
                    out.write( word+ "\n");
                    out.flush();
                    String input = in.readLine();
                    if (input.equals("Matches")){
                        System.out.println("Это правильно ");
                        break;
                    }else {
                        System.out.println("Это неверно "+ "\n");
                        System.out.println("пробовать снова");

                    }
                }
//                String word = reader.readLine();
//                out.write(word + "\n");
//                out.flush();
//                String serverWord = in.readLine();
//                System.out.println(serverWord);
            } finally {
                System.out.println("Клиент был закрыт...");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }

    }

}
