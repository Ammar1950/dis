package com.company;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Server {


    private static Socket clientSocket;
    private static ServerSocket server;
    private static BufferedReader in;
    private static BufferedWriter out;

    private  static int number = new Random().nextInt(10);
    public static void main(String[] args) {
        try {
            try {
                server = new ServerSocket(10000);
                System.out.println("Сервер запущен!");
                clientSocket = server.accept();
                try {
                    /*
                     * if input == number
                     *   return matches
                     * else doesn't match
                     * */
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
                    System.out.println(number+"\n");
                    for (int i = 0; i < 3; i++) {
                        String word = in.readLine();
                        System.out.println(word);
                        if (word.equals(String.valueOf(number))) {
                            out.write("Matches"+"\n");
                            out.flush();
                            break;
                        } else {
                            out.write("Doesn't Match"+"\n");
                        }
                        //out.write("Привет, это Сервер! Подтверждаю, вы написали : " + word + "\n");
                        out.flush();
                    }
                } finally {
                    clientSocket.close();
                    in.close();
                    out.close();
                }
            } finally {
                System.out.println("Сервер закрыт!");
                server.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}
