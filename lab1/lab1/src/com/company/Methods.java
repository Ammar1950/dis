package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Methods{

  private static final List<Data> list = new ArrayList<>();
  private static final List<Data> listSecondFile = new ArrayList<>();
  private static double meanX;
  private static double meanY;

  public static double findMeanX() {
    double sum = 0;
    for (Data x: list) {
        sum += x.getCLOSE();
    }
    meanX = sum /list.size();
    return meanX;
  }
  public static double findMeanXY() {
      double sum = 0;
      for (int i = 0; i < list.size(); i++) {
          sum += list.get(i).getCLOSE() * listSecondFile.get(i).getCLOSE();
      }
      return sum / list.size();
  }
  public static double findMeanY() {
      double sum = 0;
      for (Data x: listSecondFile) {
          sum += x.getCLOSE();
      }
      meanY = sum / listSecondFile.size();
      return meanY;
    }
    private static double findMeanXDiff() {
      double sum = 0;
      for(Data x: list) {
          sum += (x.getCLOSE()*x.getCLOSE() - meanX * meanX);
      }
      return sum / list.size();
    }
    private static double findMeanYDiff() {
        double sum = 0;
        for(Data x: listSecondFile) {
            sum += (x.getCLOSE()*x.getCLOSE() - meanY * meanY);
        }
        return sum / listSecondFile.size();
    }

    public static double findSD_X() {
        return Math.sqrt(findMeanXDiff());
    }
    public static double findSD_Y() {
      return Math.sqrt(findMeanYDiff());
    }

  public static void readFilesFromData(String path ) throws IOException {

      BufferedReader br = new BufferedReader(new FileReader(path));
      br.readLine();
      String st;
      while ((st = br.readLine())  != null){

          String[] arr;
          arr = st.split(";");

          list.add(new Data(arr[0],arr[1],Integer.parseInt(arr[2]),Double.parseDouble(arr[4])));
      }
  }

  public static void readFilesFromData1(String path) throws IOException{

      BufferedReader br = new BufferedReader(new FileReader(path));
      br.readLine();
      String st;

      while ((st = br.readLine())  != null){

          String[] arr;
          arr = st.split(";");

          listSecondFile.add(new Data(arr[0],arr[1],Integer.parseInt(arr[2]), Double.parseDouble(arr[4])));
      }
  }


}
