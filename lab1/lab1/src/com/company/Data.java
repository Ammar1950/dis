package com.company;

public class Data {

    //<TICKER>,<PER>,<DATE>,<TIME>,<CLOSE>

    String TICKER;
    String PER;
    int DATA;
    int TIME;
    double CLOSE;

    public Data(String TICKER, String PER, int DATA, double CLOSE) {
        this.TICKER = TICKER;
        this.PER = PER;
        this.DATA = DATA;
        this.CLOSE = CLOSE;
    }

    public String getTICKER() {
        return TICKER;
    }

    public String getPER() {
        return PER;
    }

    public int getDATA() {
        return DATA;
    }

    public int getTIME() {
        return TIME;
    }

    public double getCLOSE() {
        return CLOSE;
    }

    @Override
    public String toString() {
        return "Data{" +
                "TICKER='" + TICKER + '\'' +
                ", PER=" + PER +
                ", DATA=" + DATA +
                ", CLOSE=" + CLOSE +
                '}';
    }

}
