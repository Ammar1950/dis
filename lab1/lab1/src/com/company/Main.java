package com.company;

import java.io.IOException;


public class Main {
    static double meanX;
    static double meanY;
    static double meanXY;
    static double sdX;
    static double sdY;
    public static void main(String[] args){
        long start = System.currentTimeMillis();

        if (args.length < 2 || args.length > 3){
            System.out.println("Error 501");
            System.exit(-1);
        }

        Thread thread1 = new Thread() {

            public synchronized void run() {
                try {
                    Methods.readFilesFromData( args[0]);
                    meanX = Methods.findMeanX();
                    sdX = Methods.findSD_X();
                //    Methods.printList();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread thread2 = new Thread() {
            public synchronized void run() {
                try {
                    Methods.readFilesFromData1(args[1]);
                    meanY = Methods.findMeanY();
                    sdY = Methods.findSD_Y();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            System.out.println("Thread interrupted");
            System.exit(0);
        }
        meanXY = Methods.findMeanXY();
        System.out.println((meanXY - meanX * meanY) / (sdX * sdY));


        long end = System.currentTimeMillis();
        long elapsedTime = end - start;

        System.out.println(elapsedTime);
    }
}

