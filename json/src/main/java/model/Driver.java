package model;

public class Driver {



    private  int number;
    private String name;
    private int car;
    private int IdDriver;


    public  Driver(){
    }

    public Driver(int number, String name, int car, int IdDriver) {
        this.number = number;
        this.name = name;
        this.car = car;
        this.IdDriver= IdDriver;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCar() {
        return car;
    }

    public void setCar(int car) {
        this.car = car;
    }

    public int getIdDriver() {
        return IdDriver;
    }

    public void setIdDriver(int idDriver) {
        IdDriver = idDriver;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "number=" + number +
                ", name='" + name + '\'' +
                ", car=" + car +
                ", IdDriver=" + IdDriver +
                '}';
    }
}
