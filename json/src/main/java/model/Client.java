package model;

public class Client {


    private int clientId;
    private String phone;
    private String name;

    public Client(int clientId, String phone, String name) {
        this.clientId = clientId;
        this.phone = phone;
        this.name = name;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Client{" +
                "clientId=" + clientId +
                ", phone='" + phone + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
