package model;

public class Car {

    private int Id;
    private  String number;
    private String model;

    public Car(int id, String number, String model) {
        Id = id;
        this.number = number;
        this.model = model;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Car{" +
                "Id='" + Id + '\'' +
                ", number='" + number + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
