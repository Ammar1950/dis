package model;

public class Trips {


    private Trip trip;

    public Trips(Trip trip) {
        this.trip = trip;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    @Override
    public String toString() {
        return "Trips{" +
                "trip=" + trip +
                '}';
    }
}
