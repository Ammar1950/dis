package model;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.*;

public class Main {

    public static void main(String[] args) {


        Car car = new Car(1,"M111MM116","");
        Driver driver = new Driver(1,"HezAm",33333,1);
        Driver driver1  =new Driver();
        driver1.setIdDriver(2);driver1.setCar(66666);driver1.setName("Ahmed");driver1.setNumber(895032258);
        Client client = new Client(1,"89526453245","Ali");
        Trip trip = new Trip("2021-09-13",car, client, driver);

        Trips trips = new Trips(trip);

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(new File("C:\\Users\\najia\\IdeaProjects\\json\\src\\main\\java\\JSON\\DB.json" ),trips);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
